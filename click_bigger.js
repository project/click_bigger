// borrowing heavily from http://jqueryfordesigners.com/image-cross-fade-transition/
$.fn.click_bigger = function(options){
	return this.each(function(index){
		var $$ = $(this);
		// get path to larger image
		if (isArray(Drupal.settings.click_bigger.largerImage)) {
			bigger = Drupal.settings.click_bigger.largerImage[index];
		}
		else {
				bigger = Drupal.settings.click_bigger.largerImage;
		}
		h = $$.height();
		w = $$.width();
		// wrap in a span
		$$.wrap('<span style="position: relative; display: block"></span>')
		
			.css('cursor', 'pointer')
			// change selector to parent (the new span)
			.parent()
			// prepend a new image inside the span
			.prepend('<img>')
			//change the selector to the new image
			.find('img:first-child')
			.css('display', 'none')
			.addClass('first')
			// set the image src to the bigger image path
			.attr('src', bigger);
			$$.click( function(){			
				$$.prev().show();	
			  $(this).hide();
			});
			$$.prev().click( function(){			
				$$.prev().hide();	
				$$.show();
			});
		

	});
};

// Not only when the DOM is ready, but when the images have finished loading,
// important, but subtle difference to $(document).ready();
$(window).bind('load', function () {
  // run the cross fade plugin against selector
  $('img.click_bigger').click_bigger();
});

function isArray(obj) {
    return obj.constructor == Array;
}